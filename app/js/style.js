$(document).ready(function() {
  /*faq open block */
  $('.faq-head').on('click', function() {
    $(this)
      .parent()
      .find('.faq-body')
      .slideToggle(300);

    $(this)
      .find('.btn_circle_close')
      .toggleClass('active');
  });

  /*scroll plugin */
  $('.scroll-block').scrollbar();

  /*modal window */

  $('.modal-open').on('click', function() {
    var currentModal = $(this).attr('href');
    $('.modal-window').each(function() {
      var currentId = '#' + $(this).attr('id');
      if (currentId === currentModal) {
        $(this)
          .fadeIn(400)
          .addClass('modal-show');
        $('body').addClass('overflow');
      }
    });

    if ($(this).hasClass('video-overlay')) {
      $('.modal-content')
        .find('video')
        .get(0)
        .play();
    }
  });

  $('.modal-close').on('click', function() {
    $('.modal-window')
      .fadeOut(400)
      .removeClass('modal-show');
    $('body').removeClass('overflow');

    $('.modal-content')
      .find('video')
      .get(0)
      .pause();
  });

  $('.modal-window').on('click touchstart', function(event) {
    var element = $('.modal-content');
    if (!$(event.target).closest(element).length) {
      $('.modal-window')
        .fadeOut(400)
        .removeClass('modal-show');
      $('body').removeClass('overflow');

      $('.modal-content')
        .find('video')
        .get(0)
        .pause();
    }
  });

  /*menu */
  $('header li a').on('click', function(event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id = $(this).attr('href'),
      //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;

    //анимируем переход на расстояние - top за 1000 мс
    $('html, body')
      .stop()
      .animate(
        {
          scrollTop: top,
          easing: 'easein',
        },
        1000
      );
  });
  $('.menu-mobile').on('click', function() {
    $(this).toggleClass('active');
    $('nav').slideToggle();
    $('body').toggleClass('overflow');
  });

  /*slider photo */

  Revealator.effects_padding = '-150';

  /* */

  var ctrl = new ScrollMagic.Controller({});

  var tl1 = new TimelineMax();
  tl1
    .fromTo(
      $('#about-subsection h6'),
      0.5,
      { x: '95%' },
      { x: '-85%', ease: Linear.easeOut }
    )
    .to(
      $('#about-subsection h6'),
      0.5,
      {
        y: '250%',
        ease: Linear.easeOut,
      },
      '-=0.5'
    );

  var scene1 = new ScrollMagic.Scene({
    triggerElement: '#about-subsection',
    triggerHook: 1,
    duration: '150%',
  })
    .setTween(tl1)
    .addTo(ctrl);

  var ctrl2 = new ScrollMagic.Controller({});

  var tl2 = new TimelineMax();
  tl2
    .fromTo(
      $('#schedule-subsection h6'),
      0.5,
      { x: '95%' },
      { x: '-95%', ease: Linear.easeOut }
    )
    .to(
      $('#schedule-subsection h6'),
      0.5,
      {
        y: '200%',
        ease: Linear.easeOut,
      },
      '-=0.5'
    );

  var scene2 = new ScrollMagic.Scene({
    triggerElement: '#schedule-subsection',
    triggerHook: 1,
    duration: '150%',
  })
    .setTween(tl2)
    .addTo(ctrl2);

  var ctrl3 = new ScrollMagic.Controller({});

  var tl3 = new TimelineMax();
  tl3
    .fromTo(
      $('#location-subsection h6'),
      0.5,
      { x: '95%' },
      { x: '-95%', ease: Linear.easeOut }
    )
    .fromTo(
      $('#location-subsection h6'),
      0.5,
      {
        y: '-50%',
      },
      {
        y: '300%',
        ease: Linear.easeOut,
      },
      '-=0.5'
    );

  var scene3 = new ScrollMagic.Scene({
    triggerElement: '#location-subsection',
    triggerHook: 1,
    duration: '250%',
  })
    .setTween(tl3)
    .addTo(ctrl3);

  var ctrl4 = new ScrollMagic.Controller({});

  var tl4 = new TimelineMax();
  tl4
    .fromTo(
      $('#team-subsection h6'),
      2.5,
      { x: '95%' },
      { x: '-90%', ease: Linear.easeOut }
    )
    .to(
      $('#team-subsection h6'),
      2.5,
      {
        y: '300%',
        ease: Linear.easeOut,
      },
      '-=2.5'
    );

  var scene4 = new ScrollMagic.Scene({
    triggerElement: '#team-subsection',
    triggerHook: 0.6,
    duration: '150%',
  })
    .setTween(tl4)
    .addTo(ctrl4);
});

$(window).on('load', function() {
  /*instagram stop*/

  var target = $('footer').offset().top,
    winHeight = $(window).height(),
    scrollToElem = target - winHeight,
    fixedMenu = $('.fixed-menu');
  $(window).scroll(function() {
    var winScrollTop = $(this).scrollTop();
    if (winScrollTop > scrollToElem) {
      fixedMenu.addClass('bottom-fixed').removeClass('top-fixed');
    } else {
      fixedMenu.removeClass('bottom-fixed').addClass('top-fixed');
    }
  });

  var swiper = new Swiper('.swiper-container', {
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  var elm = document.querySelector('#fixed-scroll');
  setTimeout(function() {
    var ms = new MenuSpy(elm, {
      threshold: 100,
    });
  }, 500);

  var $window = $(window),
    lastScrollTop = 0;

  $window.on('scroll', function() {
    var elm = document.querySelector('#fixed-scroll');
    var top = $window.scrollTop();
    if (lastScrollTop > top) {
      var ms = new MenuSpy(elm, {
        threshold: 450,
      });
    } else if (lastScrollTop < top) {
      var ms = new MenuSpy(elm, {
        threshold: 100,
      });
    }
    lastScrollTop = top;
  });
});
